# roadsterConnectorReference

Reference for electrical connectors used in the Mazda Miata.

# NA6

| purpose                       | Manufacturer | connector shell | terminal                  | seal | mating connector    |
|-------------------------------|--------------|-----------------|---------------------------|------|---------------------|
| ECU board header              | [TE][]       | [174917-6][]    |                           |      | 174516-1 174515-1   |
| ECU 26 position connector     | [TE][]       | [174516-1][]    | [173631-2][] [175094-1][] |      | 174917-6 3-178780-6 |
| ECU 22 position connector     | [TE][]       | [174515-1][]    | [173631-2][] [175094-1][] |      | 174917-6 3-178780-6 |

# NA8

| purpose                       | Manufacturer | connector shell | terminal                  | seal | mating connector    |
|-------------------------------|--------------|-----------------|---------------------------|------|---------------------|
| NA8 ECU board header          | [TE][]       | [3-178780-6][]  |                           |      | 174516-1 174515-1   |
| ECU 26 position connector     | [TE][]       | [174516-1][]    | [173631-2][] [175094-1][] |      | 174917-6 3-178780-6 |
| ECU 22 position connector     | [TE][]       | [174515-1][]    | [173631-2][] [175094-1][] |      | 174917-6 3-178780-6 |

# Other common connectors

| purpose                       | Manufacturer | connector shell | terminal                                                | seal                                                    | mating connector    |
|-------------------------------|--------------|-----------------|---------------------------------------------------------|---------------------------------------------------------|---------------------|
| Toyota ITB TPS connector      | [Sumitomo][] | 90980-10845     | [8100-0460][] [8100-1344][] [8100-0461][] [8100-0594][] | [7165-0842][] [7165-0349][] [7165-0395][] [7165-0351][] |                     |
| GM IACV connector             | Delphi       | [12162188][]    | [12124075][]                                            |                                                         |                     |

[Sumitomo]: https://www.sws.co.jp/en/index.html
[8100-0460]: http://prd.sws.co.jp/components/en/detail.php?number_s=81000460
[8100-1344]: http://prd.sws.co.jp/components/en/detail.php?number_s=81001344
[8100-0461]: http://prd.sws.co.jp/components/en/detail.php?number_s=81000461
[8100-0594]: http://prd.sws.co.jp/components/en/detail.php?number_s=81000594
[7165-0842]: http://prd.sws.co.jp/components/en/detail.php?number_s=71650842 
[7165-0349]: http://prd.sws.co.jp/components/en/detail.php?number_s=71650349 
[7165-0395]: http://prd.sws.co.jp/components/en/detail.php?number_s=71650395
[7165-0351]: http://prd.sws.co.jp/components/en/detail.php?number_s=71650351

[delphi]: https://www.mouser.com/aptiv/
[12162188]: https://www.mouser.com/Search/Refine.aspx?Keyword=12162188
[12124075]: https://www.mouser.com/Search/Refine.aspx?Keyword=12124075

[TE]: https://www.te.com/
[174917-6]: https://www.te.com/global-en/product-174917-6.html
[173631-2]: https://www.te.com/global-en/product-173631-2.html
[175094-1]: https://www.te.com/global-en/product-175094-1.html
[174516-1]: https://www.te.com/global-en/product-174516-1.html
[174515-1]: https://www.te.com/global-en/product-174515-1.html
[3-178780-6]: https://www.te.com/global-en/product-3-178780-6.html

# Seals
for seals that are difficult to source [Aptiv provides a nice reference of Delphi parts](http://ecat.aptiv.com/docs/default-source/ecatalog-documents/delphi-liquid-silicone-and-elastomer-technologies-pdf.pdf).

# License
This reference is presented under a Creative Commons 0 public domain license.

<p xmlns:dct="http://purl.org/dc/terms/">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span rel="dct:publisher" resource="[_:publisher]">the person who associated CC0</span>
  with this work has waived all copyright and related or neighboring
  rights to this work.
</p>
